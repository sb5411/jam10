// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ActorComponent.h"
#include "PlatformMove.generated.h"



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class JAM9_API UPlatformMove : public UActorComponent 
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UPlatformMove();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;



public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(VisibleAnywhere)
		class UBoxComponent* MyComp;

	UFUNCTION()
		void OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
public:
	AActor* owner;
	FVector InitialPos, InitialScale, currentPos, currentScale;
	FRotator InitialRot, currentRot;

	UPROPERTY(EditAnywhere)
		float xMove;
	UPROPERTY(EditAnywhere)
		float yMove;
	UPROPERTY(EditAnywhere)
		float zMove;
	UPROPERTY(EditAnywhere)
		float maxD;
	UPROPERTY(EditAnywhere)
		float xRot;
	UPROPERTY(EditAnywhere)
		float yRot;
	UPROPERTY(EditAnywhere)
		float zRot;
	UPROPERTY(EditAnywhere)
		float xScale;
	UPROPERTY(EditAnywhere)
		float yScale;
	UPROPERTY(EditAnywhere)
		float zScale;

	void SetTranslation(float);
	void SetRotation(float);
	void SetScaling(float);
	void CheckDistance();
	void ChangeDirection();
};
