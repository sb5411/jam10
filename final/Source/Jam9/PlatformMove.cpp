// Fill out your copyright notice in the Description page of Project Settings.

#include "PlatformMove.h"
#include "Components/BoxComponent.h"

// Sets default values for this component's properties
UPlatformMove::UPlatformMove()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	owner = GetOwner();

	// Use a sphere as a simple collision representation
	MyComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	MyComp->SetSimulatePhysics(true);
	MyComp->SetNotifyRigidBodyCollision(true);

	MyComp->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
	
	


	//mesh
	//owner = GetComponentClassCanReplicate();
	//You only need to register the OnHit function
	
	
}


// Called when the game starts
void UPlatformMove::BeginPlay()
{
	Super::BeginPlay();
	//owner = GetOwner();
	InitialPos = owner->GetActorLocation();
	InitialRot = owner->GetActorRotation();
	InitialScale = owner->GetActorScale();
	
}

void UPlatformMove::OnCompHit(UPrimitiveComponent * HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
}




// Called every frame
void UPlatformMove::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	CheckDistance();
	SetTranslation(DeltaTime);
	SetRotation(DeltaTime);
	SetScaling(DeltaTime);
}

void UPlatformMove::SetTranslation(float delta)
{
	FVector translation = currentPos + FVector(xMove*delta, yMove*delta, zMove*delta);
	owner->SetActorLocation(translation);
}

void UPlatformMove::SetRotation(float delta)
{
	FRotator rotation = currentRot + FRotator(zRot*delta, yRot*delta, xRot*delta);
	owner->SetActorRotation(rotation);
}

void UPlatformMove::SetScaling(float delta)
{
	FVector scaling = currentScale + FVector(xScale*delta + yScale * delta + zScale * delta);
	owner->SetActorScale3D(scaling);
}



void UPlatformMove:: CheckDistance() 
{
	currentPos = owner->GetActorLocation();
	currentScale = owner->GetActorScale();
	currentRot = owner->GetActorRotation();
	float xdiff =FGenericPlatformMath::Abs(InitialPos.X - currentPos.X);
	float ydiff =FGenericPlatformMath::Abs(InitialPos.Y - currentPos.Y);
	float zdiff =FGenericPlatformMath::Abs(InitialPos.Z - currentPos.Z);

	if (xdiff > maxD|| ydiff> maxD|| zdiff> maxD)
	{
		ChangeDirection();
	}
}

void UPlatformMove::ChangeDirection() 
{
	xMove *= -1;
	yMove *= -1;
	zMove *= -1;
	xRot *= -1;
	yRot *= -1;
	zRot *= -1;
	xScale *= -1;
	yScale *= -1;
	zScale *= -1;
}
