// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Jam9GameMode.h"
#include "Jam9Character.h"
#include "UObject/ConstructorHelpers.h"

AJam9GameMode::AJam9GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
