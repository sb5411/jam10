// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ModifiedPlatformActor.generated.h"

UCLASS()
class JAM9_API AModifiedPlatformActor : public AActor
{
	GENERATED_BODY()
		
	
public:
	// Sets default values for this actor's properties
	AModifiedPlatformActor();

	UPROPERTY(VisibleAnywhere)
		class UBoxComponent* MyComp;

	UPROPERTY(VisibleDefaultsOnly, Category = JustTile)
		class UStaticMeshComponent*  MyTile;

	UFUNCTION(BlueprintCallable, Category = Buttonfunc)
		bool CheckHitForOtherTileMove();

	UFUNCTION(BlueprintCallable, Category = SettingActorPhy)
		void SetMyActorPhysics();

	//UFUNCTION(BlueprintCallable, Category = CheckHowManyHits)
	//	UINT CheckNoOfHits();

	bool ShouldOtherTileMove;
	bool ShouldOtherTileMoveHandleTwo;
	//	void MoveRight();

		//void TakeDmg_Implementation();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
private:
	int32 tempInt = 0;

};
