// Fill out your copyright notice in the Description page of Project Settings.

#include "ModifiedPlatformActor.h"
#include "Components/BoxComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/SphereComponent.h"
#include "Components/BoxComponent.h"
#include "Engine/CollisionProfile.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "Engine.h"



// Sets default values
AModifiedPlatformActor::AModifiedPlatformActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use a sphere as a simple collision representation
	static ConstructorHelpers::FObjectFinder<UStaticMesh> TileMesh(TEXT("/Game/StarterContent/Shapes/Shape_Cube"));

	MyTile = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TileComponent"));

	MyComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	//MyComp->SetSimulatePhysics(true);
	MyComp->SetNotifyRigidBodyCollision(true);
	MyComp->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
	MyComp->OnComponentHit.AddDynamic(this, &AModifiedPlatformActor::OnCompHit);


	MyTile->SetWorldScale3D(FVector(8.0f, 8.0f, 0.5f));
	MyTile->SetStaticMesh(TileMesh.Object);
	MyTile->SetNotifyRigidBodyCollision(true);
	MyTile->SetGenerateOverlapEvents(true);
	// Set as root component
	RootComponent = MyTile;
	MyComp->bHiddenInGame = false;

	MyComp->SetupAttachment(RootComponent);

	
	MyComp->SetRelativeLocation(FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + 110.f));

	ShouldOtherTileMove = false;
	ShouldOtherTileMoveHandleTwo = false;

	tempInt = 0;

	//MyComp->OnComponentHit.AddDynamic(this, &AModifiedPlatformActor::OnCompHit);
	//	// Use a sphere as a simple collision representation
	//MyComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	//MyComp->SetSimulatePhysics(true);
	//MyComp->SetNotifyRigidBodyCollision(true);

	//MyComp->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
	

	//// Set as root component
	//RootComponent = MyComp;
}


// Called when the game starts or when spawned
void AModifiedPlatformActor::BeginPlay()
{
	Super::BeginPlay();
	//MyComp->OnComponentBeginOverlap.AddDynamic(this, &AModifiedPlatformActor::ChangeCubeColor);


	
}

// Called every frame
void AModifiedPlatformActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AModifiedPlatformActor::SetMyActorPhysics()
{
	if (ShouldOtherTileMove == false && ShouldOtherTileMoveHandleTwo==true)
	{
		//MyTile->SetSimulatePhysics(false);
		MyTile->SetSimulatePhysics(true);

	}
	else if (ShouldOtherTileMove == true && ShouldOtherTileMoveHandleTwo == false && tempInt>0)
	{
		ShouldOtherTileMove = false;
		MyTile->SetSimulatePhysics(true);
		ShouldOtherTileMoveHandleTwo = true;
		tempInt = 0;
	}
}
void AModifiedPlatformActor::OnCompHit(UPrimitiveComponent * HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("I Hit: %s"), *OtherActor->GetName()));

		
		//MyTile->SetSimulatePhysics(true);
		tempInt = tempInt + 1;
		ShouldOtherTileMove = true;
	}

}

bool AModifiedPlatformActor::CheckHitForOtherTileMove()
{
	if (tempInt == 1)
	{
		return true;
	}
	else
		return false;
	
}

